# 2.0.2 - Dec. 10, 2017
- Movement speeds minimum is now 200 mm/min (previously min. 100) - due to firmware limitations 
- If settings are changed since last load, display warning to refresh
- Disable heating while laser is on
- Better fill, now sequential

# 2.0.1 - Sept. 5, 2017
- Disabled zoom and set minimum size for layout

# 2.0.0 - Sept. 1, 2017
- Initial release